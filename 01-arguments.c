#include <stdio.h>

int main(int argc, char** argv) {
    printf("Given %d arguments.\n", argc);

    int index = 0;
    while (index < argc) {
        printf("Argument %d: %s\n", index, argv[index]);
        index = index + 1;
    }
}

